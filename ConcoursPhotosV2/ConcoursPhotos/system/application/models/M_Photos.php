<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Photos extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_photos($competition) {

        $query = $this->db->select("competition.idCo AS 'idComp'")
                ->select('photo.idP, photo.idCo, photo.titre, photo.nomFichier, photo.ordreProjection, photo.total, photo.classement,
                competition.nom, competition.dossierStockage, competition.date')
                ->from('photo, competition')
                ->where('photo.idCo', $competition)
                ->where('competition.idCo', $competition)
                ->get();
        return $query->result_array();
       
    }

}

