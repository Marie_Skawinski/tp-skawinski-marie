<?php

defined('BASEPATH') or exit('No direct script access allowed');

class C_Connexion extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Connexion');
        $this->load->model('M_Photos');
        session_start();
    }

    public function index()
    {

        if (!isset($_SESSION['user'])) {
            $data['titre'] = "Accueil | Concours Photos";

            $this->load->view('V_Connexion', $data);


        }else{
            $_SESSION['user'] = $_SESSION['user'];
            $data['titre'] = "Accueil | Concours Photos";
            $page = $this->load->view('V_Accueil', $data, true);
            $this->load->view('template/V_Template', array('contenu' => $page));

        }

        
    }


    public function logout()
    {
        //removing session  

        session_destroy();
        $_SESSION['user'] = null;
        redirect("C_Connexion");

    }

    public function afficher_photos($competition)
    {
        $array_resultat = $this->M_Photos->get_photos($competition);
        return $array_resultat;
    }

    public function return_accueil()
    {
        $data['titre'] = "Accueil | Concours Photos";
        $page = $this->load->view('V_Accueil', $data, true);
        $this->load->view('template/V_Template', array('contenu' => $page));
    }
}
