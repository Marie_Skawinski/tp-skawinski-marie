<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_Import extends CI_Controller {

    public function __construct() {
        parent::__construct();
          
    }

    public function index() {

        $data['titre'] = "Compétition | Concours Photos";
        $data['titrepage'] = "Importer les données du concours en appuyant sur le bouton";
        $page = $this->load->view('V_Import', $data, true);
        $this->load->view('template/V_Template', array('contenu' => $page));

    }
    
    public function import(){

       // echo shell_exec("mysql -uroot -praspberry  concours_photo < /media/pi/Transcend1/import.sql;");

        shell_exec('ln -s /media/pi/Transcend1/images /var/www/html/ConcoursPhotos/assets/photos');
        shell_exec('ln -s /media/pi/Transcend1/import /var/www/html/ConcoursPhotos/assets');
        $file = file_get_contents(base_url('/assets/import/import.sql'), FILE_USE_INCLUDE_PATH);
        $data['test'] = $file;
/*      
        if($sortit=="oui"){
            $data['test'] = "Les données sont bien importées";
        }else{
            $data['test'] ="Il y a eu une erreur pendant l'importation";
        }
*/
        
        
        $data['titre'] = "Compétition | Concours Photos";
        $data['titrepage'] = "Importer les données du concours en appuyant sur le bouton";
        $page = $this->load->view('V_Import', $data, true);
        $this->load->view('template/V_Template', array('contenu' => $page));
    }

}