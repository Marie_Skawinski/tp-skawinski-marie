<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Photo extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Jury');
        $this->load->model('M_Photos');
        $this->load->model('M_Connexion');
        session_start();
    }

    public function index_get()
    {
    }
//-------------Lire un jury-----------------------------------------------
    public function jury_get()
    {
        $results = $this->M_Jury->lire_jury();
        $this->response($results, REST_Controller::HTTP_OK);
    }
//------------------Lire le tableau de photo, L'id et ordre de projection---------------------------------------
    public function photos_get($idcomp)
    {
        $results = $this->M_Photos->get_photos_by_comp($idcomp);
        $this->response($results, REST_Controller::HTTP_OK);
    }
//---------------Se connecter au site--------------------------------------
    public function connexion_post()
    {

        $results = array("isSuccess" => false, "error" => "HTTP_OK");


        if ($_SERVER['REQUEST_METHOD'] == "POST") {

            $login = $this->_post_args['login'];

            $pass = $this->_post_args['pass'];

            if (($pass != '') && ($login != '')) {

                $pass = hash("sha256", $pass);         

                $user = $this->M_Connexion->get_user($login, $pass);
        
                
                if($user != null)
                {
                    
                    $_SESSION['user'] = $user[0]["login"] ;

                    $results["isSuccess"] = true ;

                }

            }

            $this->response($results, REST_Controller::HTTP_OK);
        }
        
    }
//--------------------Ajouter un jury-------------------------------------
    public function index_post()
    {

        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            $nom = $this->_post_args['txtNomJury'];

            $prenom = $this->_post_args['txtPrenomJury'];
            if (($nom != '') && ($prenom != '')) {
                $res =  $this->M_Jury->ajouter_jury($nom, $prenom);

                $this->response($res, REST_Controller::HTTP_CREATED);
            }
        }
    }
    public function index_delete($id = '')
    {
    }

    public function index_put($id = '')
    {
    }
}
