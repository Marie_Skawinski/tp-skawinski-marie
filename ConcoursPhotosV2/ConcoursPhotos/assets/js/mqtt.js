

//---------------------------------------- Variables globales ------------------------------------------------//
if(configGlobale.enabled = true){
    var base_url = configGlobale.base_url;
    var host_mqtt = configGlobale.host_mqtt;
}
var total = 0;
var phase = 1;
var cpt = 1;
var cptPhotos = 0;
var arrayJury;

var arrayPhotos = [
    {
        "ordreProjection": "1",
        "lienImg": "1.PNG"
    },
    {
        "ordreProjection": "2",
        "lienImg": "2.PNG"
    },
    {
        "ordreProjection": "3",
        "lienImg": "3.PNG"
    },
    {
        "ordreProjection": "4",
        "lienImg": "4.PNG"
    },
    {
        "ordreProjection": "5",
        "lienImg": "5.png"
    }
];



//------------------------------------------- Appli -----------------------------------------------------//

function onBtnPause() {
    $src = $("#imgPause").attr('src');
    if ($src == base_url + "assets/img/iconpause.png") {
        $("#imgPause").attr('src', base_url + 'assets/img/logoplay.png');
    } else {
        $("#imgPause").attr('src', base_url + 'assets/img/iconpause.png');
    }
}

function onBtnSuiv() {
    phase = phase + 1;
    cptPhotos = 0;
    timeout();
    if (phase == 3) {
        //phase = 0;
        $("#divPSP").html('<a id="btnClassement" href="#">Classement <i class="fa fa-arrow-circle-right"></i></a>');
    }

}

function onBtnPrec() {
    phase = phase - 1;
    cptPhotos = 0;
    if (phase == 3) {
        //phase = 0;
        $("#divPSP").html('<a id="btnClassement" href="#">Classement <i class="fa fa-arrow-circle-right"></i></a>');
    }
}

function afficherJury() {
    for (var i = 0; i < arrayJury.length; i = i + 3) {
        var i3 = i + 3;
        if (typeof arrayJury[i] !== undefined) {
            $("#divJuryComp").append('<div class="div3jury" id="div3jury' + i + '">');
            for (var j = i; j < i3; j++) {
                if (typeof arrayJury[j] !== undefined) {
                    var juryid = j + 1;
                    $("#div3jury" + i).append('<p>Jury ' + juryid + ' : <label id="noteJury' + arrayJury[j]["ID"] + '" class="notes">--</label></p>');
                }
            }
            $("#divJuryComp").append('</div>');
        }
    }
}



var tailleArray = arrayPhotos.length;

function timeout() {
    if (phase == 1) {
        setTimeout(function () {
            $("#imgComp").attr('src', base_url + 'assets/img/screens/' + arrayPhotos[cptPhotos]["lienImg"]);
            cptPhotos++;
            if (cptPhotos < tailleArray) {
                timeout();
            }
        }, 2000);

    } else if (phase == 2) {
        setTimeout(function () {
            $("#imgComp").attr('src', base_url + 'assets/img/screens/' + arrayPhotos[cptPhotos]["lienImg"]);
            cptPhotos++;
            if (cptPhotos < tailleArray) {
                timeout();
            }
        }, 10000);
    }
}

function get_photos(prmcomp){

    $.ajax({
        type: "GET",
        url: base_url + 'REST/Photo/photos/' + prmcomp,
        dataType: "json",
        json: "json",
        success: onGetSuccess,
        error: onGetError   
    });

    function onGetSuccess(reponse, status) { 

        alert('cc');

    }

    function onGetError(status) {
        $("body").html(JSON.stringify(status));
    }
}

$(document).ready(function () {

    alert(base_url);


    try{

        $.ajax({
            type: "GET",
            url: base_url + "REST/Photo/jury",
            dataType: "json",
            json: "json",
            success: onGetSuccess,
            error: onGetError
        });

        function onGetSuccess(reponse, status) {
            arrayJury = reponse;
            afficherJury();

        }

        function onGetError(status) {
            $("body").html(JSON.stringify(status));
        }
    }catch(error){

    }
    //------------------------------------------- Client MQTT -----------------------------------------------------//

    client = new Paho.MQTT.Client(host_mqtt, 9001, "ConcoursPhotos");

    // set callback handlers
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;


    // connect the client
    client.connect(configMQTT);

    //------------------------------------------- Evenementiel -----------------------------------------------------//

    // called when the client connects
    function onConnect() {
        // Once a connection has been made, make a subscription and send a message.
        console.log("Connecté");
        client.subscribe("notes");
        message = new Paho.MQTT.Message('{"jury" : "1", "note" : "6"}');
        message.destinationName = "phase";
        // client.send(message);
    }

    function doFail(e) {
        console.log(e);
    }

    // called when the client loses its connection
    function onConnectionLost(responseObject) {
        if (responseObject.errorCode !== 0) {
            console.log("Connexion perdue: " + responseObject.errorMessage);
        }
    }

    // called when a message arrives
    function onMessageArrived(message) {

        console.log("Nouveau message: " + message.payloadString);
        var obj = JSON.parse(message.payloadString);

        if ($("#noteJury" + obj.jury).text() == "--") {
            total = total + Number(obj.note);
            $("#noteJury" + obj.jury).html(obj.note);
            $("#noteJury" + obj.jury).addClass("noteJury");

            if (total == 1) {
                $("#ptsComp").html(total + " point");
            } else {
                $("#ptsComp").html(total + " points");
            }
            $("#ptsComp").addClass("totalJury");
        }
    }
});


