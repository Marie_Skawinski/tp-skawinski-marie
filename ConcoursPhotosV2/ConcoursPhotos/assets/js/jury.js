if(configGlobale.enabled = true){
    var base_url = configGlobale.base_url;
}

$(document).ready(function(){
    //au démarage de la page
    $.ajax({
        type: "GET",
        url: base_url + "REST/Photo/jury",
        dataType: "json",
        json: "json",
        success: onGetSuccess,
        error: onGetError   
    });

    function onGetSuccess(reponse, status) { 
         reponse.forEach(function(element){
            $("#ligne").append("<tr><td>"+element.ID +"</td>"+"<td>"+element.Nom+"</td>"+"<td>"+element.Prenom+"</td></tr>");
    });
  
    
        
           
    }

    function onGetError(status) {
        alert('Un probléme est survenu dans le chargement des données');
    }

    //lors de l'appuie sur le bouton ajouté
    $("#formJury").submit(function(e){

        e.preventDefault();
        var postdata = $('#formJury').serialize();
        
        $.ajax({
            type: "POST",
            url: base_url + "REST/Photo/index",
            dataType: "json",
            json: "json",
            data : postdata,
            success: onGetSuccess,
            error: onGetError   
        });
    function onGetSuccess(reponse, status) { 
        $("#ligne").append("<tr><td>"+reponse[0]['ID'] +"</td>"+"<td>"+reponse[0]['Nom']+"</td>"+"<td>"+reponse[0]['Prenom']+"</td></tr>");
        
           
    }
    
    function onGetError(status) {
        alert('erreur la saisie est incorrect');
    }
    
    });
});
